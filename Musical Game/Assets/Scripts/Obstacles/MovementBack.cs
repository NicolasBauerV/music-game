﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementBack : MonoBehaviour
{
    private float speed = 15f;
    private const int SCORE_MAX = 5;
    private BoxCollider boxCollidCube;
    public Rigidbody cubeObsRb;

    // Start is called before the first frame update
    void Start()
    {
        cubeObsRb = GetComponent<Rigidbody>();
        boxCollidCube = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.y < -10)
        {
            Destroy(gameObject);
        }
        transform.Translate(Vector3.back * speed * Time.deltaTime);
    }

    private void OnTriggerStay(Collider other)
    {
        if(Input.GetKey(KeyCode.A) && other.gameObject.name == "Trigger Red")
        {
            cubeObsRb.AddForce(Vector3.up * 5, ForceMode.Impulse);
        }

        if (Input.GetKey(KeyCode.Z) && other.gameObject.name == "Trigger Blue")
        {
            cubeObsRb.AddForce(Vector3.up * 5, ForceMode.Impulse);
        }

        if (Input.GetKey(KeyCode.E) && other.gameObject.name == "Trigger Yellow")
        {
            cubeObsRb.AddForce(Vector3.up * 5, ForceMode.Impulse);
        }

        if (Input.GetKey(KeyCode.R) && other.gameObject.name == "Trigger Green")
        {
            cubeObsRb.AddForce(Vector3.up * 5, ForceMode.Impulse);
        }
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public List<GameObject> cube;
    private float delayRepeat = 0;
    private float rateRepeat = 0.45f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObstacles", delayRepeat, rateRepeat);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void SpawnObstacles()
    {
        int index = Random.Range(0, cube.Count);
        Instantiate(cube[index], cube[index].transform.position, cube[index].transform.rotation);
    }
}
